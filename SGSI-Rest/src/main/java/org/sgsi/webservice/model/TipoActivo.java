package org.sgsi.webservice.model;

public class TipoActivo {
	private int idTipoActivo;
	private String nombreTipo;
	
	public TipoActivo(int idTipoActivo, String nombreTipo) {
		super();
		this.idTipoActivo = idTipoActivo;
		this.nombreTipo = nombreTipo;
	}
	
	public int getIdTipoActivo() {
		return idTipoActivo;
	}
	public void setIdTipoActivo(int idTipoActivo) {
		this.idTipoActivo = idTipoActivo;
	}
	public String getNombreTipo() {
		return nombreTipo;
	}
	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}
	
	

}
