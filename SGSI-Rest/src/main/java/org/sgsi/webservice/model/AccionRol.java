package org.sgsi.webservice.model;

import java.time.LocalDateTime;

public class AccionRol {
	
	private int idAccionRol;
	private boolean permiso;
	private LocalDateTime fechaModificacion;
	private Rol idRol;
	private Accion idAccion;
	
	public AccionRol(int idAccionRol, boolean permiso, LocalDateTime fechaModificacion, Rol idRol, Accion idAccion) {
		super();
		this.idAccionRol = idAccionRol;
		this.permiso = permiso;
		this.fechaModificacion = fechaModificacion;
		this.idRol = idRol;
		this.idAccion = idAccion;
	}
	public int getIdAccionRol() {
		return idAccionRol;
	}
	public void setIdAccionRol(int idAccionRol) {
		this.idAccionRol = idAccionRol;
	}
	public boolean isPermiso() {
		return permiso;
	}
	public void setPermiso(boolean permiso) {
		this.permiso = permiso;
	}
	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public Rol getIdRol() {
		return idRol;
	}
	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}
	public Accion getIdAccion() {
		return idAccion;
	}
	public void setIdAccion(Accion idAccion) {
		this.idAccion = idAccion;
	}
	
	

}
