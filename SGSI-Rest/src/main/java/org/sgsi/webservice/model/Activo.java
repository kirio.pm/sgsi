package org.sgsi.webservice.model;

public class Activo {
	
	private int idActivo;
	private String nombreActivo;
	private String nombrePropetario;
	private Empresa idEmpresa;
	private Proceso idProceso;
	private TipoActivo idTipoProceso;

	public Activo(int idActivo, String nombreActivo, String nombrePropetario, Empresa idEmpresa, Proceso idProceso,
			TipoActivo idTipoProceso) {
		super();
		this.idActivo = idActivo;
		this.nombreActivo = nombreActivo;
		this.nombrePropetario = nombrePropetario;
		this.idEmpresa = idEmpresa;
		this.idProceso = idProceso;
		this.idTipoProceso = idTipoProceso;
	}
	public int getIdActivo() {
		return idActivo;
	}
	public void setIdActivo(int idActivo) {
		this.idActivo = idActivo;
	}
	public String getNombreActivo() {
		return nombreActivo;
	}
	public void setNombreActivo(String nombreActivo) {
		this.nombreActivo = nombreActivo;
	}
	public String getNombrePropetario() {
		return nombrePropetario;
	}
	public void setNombrePropetario(String nombrePropetario) {
		this.nombrePropetario = nombrePropetario;
	}
	public Empresa getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Empresa idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public Proceso getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(Proceso idProceso) {
		this.idProceso = idProceso;
	}
	public TipoActivo getIdTipoProceso() {
		return idTipoProceso;
	}
	public void setIdTipoProceso(TipoActivo idTipoProceso) {
		this.idTipoProceso = idTipoProceso;
	}

}
