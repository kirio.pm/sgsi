package org.sgsi.webservice.model;

public class Proceso {
	
	private int idProceso;
	private String nombre;
	private String descripcion;
	private Empresa idEmpresa;
	
	public Proceso(int idProceso, String nombre, String descripcion, Empresa idEmpresa) {
		super();
		this.idProceso = idProceso;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.idEmpresa = idEmpresa;
	}

	public int getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(int idProceso) {
		this.idProceso = idProceso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Empresa getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Empresa idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
}
