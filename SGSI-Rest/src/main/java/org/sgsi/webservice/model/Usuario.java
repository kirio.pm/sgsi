package org.sgsi.webservice.model;

import java.time.LocalDateTime;

public class Usuario {
	
	private int idUsuario;
	private String nombreUsuario;
	private String nombres;
	private String apellidos;
	private String password;
	private LocalDateTime fechaModificacion;
	private Empresa idEmpresa;
	private Rol idRol;
	
	public Usuario(int idUsuario, String nombreUsuario, String nombres, String apellidos, String password,
			LocalDateTime fechaModificacion, Empresa idEmpresa, Rol idRol) {
		super();
		this.idUsuario = idUsuario;
		this.nombreUsuario = nombreUsuario;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.password = password;
		this.fechaModificacion = fechaModificacion;
		this.idEmpresa = idEmpresa;
		this.idRol = idRol;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Empresa getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Empresa idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Rol getIdRol() {
		return idRol;
	}

	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}
	
}
