package org.sgsi.webservice.model;

public class RutaMenu {
	
	private int idRutaMenu;
	private String ruta;
	private String nombre;
	private String nombreCorto;
	private Rol idRol;
	
	public RutaMenu(int idRutaMenu, String ruta, String nombre, String nombreCorto, Rol idRol) {
		super();
		this.idRutaMenu = idRutaMenu;
		this.ruta = ruta;
		this.nombre = nombre;
		this.nombreCorto = nombreCorto;
		this.idRol = idRol;
	}

	public int getIdRutaMenu() {
		return idRutaMenu;
	}

	public void setIdRutaMenu(int idRutaMenu) {
		this.idRutaMenu = idRutaMenu;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public Rol getIdRol() {
		return idRol;
	}

	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}
	
}
