package org.sgsi.webservice.model;

import java.time.LocalDateTime;

public class Accion {
	
	private int idAccion;
	private String nombre;
	private int idModificador;
	private LocalDateTime fechaModificacion;
	
	public Accion(int idAccion, String nombre, int idModificador, LocalDateTime fechaModificacion) {
		super();
		this.idAccion = idAccion;
		this.nombre = nombre;
		this.idModificador = idModificador;
		this.fechaModificacion = fechaModificacion;
	}

	public int getIdAccion() {
		return idAccion;
	}

	public void setIdAccion(int idAccion) {
		this.idAccion = idAccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdModificador() {
		return idModificador;
	}

	public void setIdModificador(int idModificador) {
		this.idModificador = idModificador;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
	

}
