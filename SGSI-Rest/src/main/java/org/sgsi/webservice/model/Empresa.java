package org.sgsi.webservice.model;

import java.time.LocalDateTime;

public class Empresa {
	
	private int idEmpresa;
	private String nombre;
	private String nit;
	private String dui;
	private String telefono;
	private String direccion;
	private LocalDateTime fechaModificacion;
	
	public Empresa(int idEmpresa, String nombre, String nit, String dui, String telefono, String direccion,
			LocalDateTime fechaModificacion) {
		super();
		this.idEmpresa = idEmpresa;
		this.nombre = nombre;
		this.nit = nit;
		this.dui = dui;
		this.telefono = telefono;
		this.direccion = direccion;
		this.fechaModificacion = fechaModificacion;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getDui() {
		return dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
	
}
