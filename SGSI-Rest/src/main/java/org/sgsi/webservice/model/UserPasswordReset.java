package org.sgsi.webservice.model;

import java.time.LocalDateTime;

public class UserPasswordReset {
	
	private int idUserPasswordReset;
	private LocalDateTime fechaModificacion;
	private String passwordAnterior;
	private String passwordNuevo;
	
	public UserPasswordReset(int idUserPasswordReset, LocalDateTime fechaModificacion, String passwordAnterior,
			String passwordNuevo) {
		super();
		this.idUserPasswordReset = idUserPasswordReset;
		this.fechaModificacion = fechaModificacion;
		this.passwordAnterior = passwordAnterior;
		this.passwordNuevo = passwordNuevo;
	}

	public int getIdUserPasswordReset() {
		return idUserPasswordReset;
	}

	public void setIdUserPasswordReset(int idUserPasswordReset) {
		this.idUserPasswordReset = idUserPasswordReset;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getPasswordAnterior() {
		return passwordAnterior;
	}

	public void setPasswordAnterior(String passwordAnterior) {
		this.passwordAnterior = passwordAnterior;
	}

	public String getPasswordNuevo() {
		return passwordNuevo;
	}

	public void setPasswordNuevo(String passwordNuevo) {
		this.passwordNuevo = passwordNuevo;
	}
	
}
