package org.sgsi.services;

import java.util.List;

import org.sgsi.dao.EmpresaDao;
import org.sgsi.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("empresaService")
@Transactional
public class EmpresaServiceImpl implements EmpresaService{
	
	@Autowired
	private EmpresaDao dao;
	
	@Override
	public List<Empresa> empresas() {
		return dao.empresas();
	}
}
