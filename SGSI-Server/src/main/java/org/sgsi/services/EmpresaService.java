package org.sgsi.services;

import java.util.List;

import org.sgsi.model.Empresa;

public interface EmpresaService {
	public List<Empresa> empresas();
}
