package org.sgsi.dao;

import java.util.List;

import org.sgsi.model.Empresa;
public interface EmpresaDao {
	public List<Empresa> empresas();
}
