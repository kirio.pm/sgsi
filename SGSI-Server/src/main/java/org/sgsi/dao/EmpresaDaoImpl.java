package org.sgsi.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.sgsi.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


@Repository("empresaDAO")
public class EmpresaDaoImpl implements EmpresaDao {
	
	@Autowired
	private SessionFactory sessionFac;

	@Override
	public List<Empresa> empresas() {
		return sessionFac.getCurrentSession().createQuery("From Empresa", Empresa.class).getResultList();
	}
	
	

}
