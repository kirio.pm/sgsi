package org.sgsi.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="TIPO_ACTIVO",schema="public")
public class TipoActivo {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbtipoactivo_idtipoactivo_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbtipoactivo_idtipoactivo_seq",
            sequenceName = "tbtipoactivo_idtipoactivo_seq",
            allocationSize = 1
    )
    @Column(name="id_idtipoactivo", unique=true, nullable=false)
	private int idTipoActivo;

	@Column(name="nombreTipo")
	private String nombreTipo;
	
	public int getIdTipoActivo() {
		return idTipoActivo;
	}
	public void setIdTipoActivo(int idTipoActivo) {
		this.idTipoActivo = idTipoActivo;
	}
	public String getNombreTipo() {
		return nombreTipo;
	}
	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}
	
	

}
