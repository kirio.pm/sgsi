package org.sgsi.model;

import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;


@Entity
@Table(name="ACCION",schema="public")
public class Accion {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbaccion_idaccion_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbaccion_idaccion_seq",
            sequenceName = "tbaccion_idaccion_seq",
            allocationSize = 1
    )
    @Column(name="id_accion", unique=true, nullable=false)
	private int idAccion;

	@Column(name="nombre")
	private String nombre;

	@Column(name="id_modificador")
	private int idModificador;

	@Column(name = "fecha_modificacion")  
	private LocalDateTime fechaModificacion;
	
	public int getIdAccion() {
		return idAccion;
	}

	public void setIdAccion(int idAccion) {
		this.idAccion = idAccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdModificador() {
		return idModificador;
	}

	public void setIdModificador(int idModificador) {
		this.idModificador = idModificador;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
	

}
