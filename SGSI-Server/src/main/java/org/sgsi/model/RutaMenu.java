package org.sgsi.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="RUTA_MENU",schema="public")
public class RutaMenu {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbrutamenu_idrutamenu_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbrutamenu_idrutamenu_seq",
            sequenceName = "tbrutamenu_idrutamenu_seq",
            allocationSize = 1
    )
    @Column(name="id_rutamenu", unique=true, nullable=false)
	private int idRutaMenu;

	@Column(name="ruta")
	private String ruta;
	
	@Column(name="nombre")
	private String nombre;

	@Column(name="nombreCorto")
	private String nombreCorto;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_rol")
	private Rol idRol;
	

	public int getIdRutaMenu() {
		return idRutaMenu;
	}

	public void setIdRutaMenu(int idRutaMenu) {
		this.idRutaMenu = idRutaMenu;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public Rol getIdRol() {
		return idRol;
	}

	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}
	
}
