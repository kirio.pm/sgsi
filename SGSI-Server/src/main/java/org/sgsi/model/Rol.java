package org.sgsi.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="ROL",schema="public")
public class Rol {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbrol_idrol_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbrol_idrol_seq",
            sequenceName = "tbrol_idrol_seq",
            allocationSize = 1
    )
    @Column(name="id_rol", unique=true, nullable=false)
	private int idRol;

	@Column(name="nombre")
	private String nombre;

	@Column(name="descripcion")
	private String descripcion;

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	

}
