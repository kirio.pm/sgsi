package org.sgsi.model;

import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;


@Entity
@Table(name="ACCION_ROL",schema="public")
public class AccionRol {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbaccionrol_idaccionrol_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbaccionrol_idaccionrol_seq",
            sequenceName = "tbaccionrol_idaccionrol_seq",
            allocationSize = 1
    )
    @Column(name="id_accion_rol", unique=true, nullable=false)
    private int idAccionRol;

	@Column(name="nombre")
	private boolean permiso;

	@Column(name = "fecha_modificacion")
	private LocalDateTime fechaModificacion;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_rol")
	private Rol idRol;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_accion")
	private Accion idAccion;
	
	public int getIdAccionRol() {
		return idAccionRol;
	}
	public void setIdAccionRol(int idAccionRol) {
		this.idAccionRol = idAccionRol;
	}
	public boolean isPermiso() {
		return permiso;
	}
	public void setPermiso(boolean permiso) {
		this.permiso = permiso;
	}
	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public Rol getIdRol() {
		return idRol;
	}
	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}
	public Accion getIdAccion() {
		return idAccion;
	}
	public void setIdAccion(Accion idAccion) {
		this.idAccion = idAccion;
	}
	
	

}
