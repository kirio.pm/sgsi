package org.sgsi.model;

import java.time.LocalDateTime;

import javax.persistence.*;

@Entity
@Table(name="USUARIO",schema="public")
public class Usuario {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbusuario_idusuario_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbusuario_idusuario_seq",
            sequenceName = "tbusuario_idusuario_seq",
            allocationSize = 1
    )
    @Column(name="id_usuario", unique=true, nullable=false)
	private int idUsuario;

	@Column(name="nombre_usuario")
	private String nombreUsuario;

	@Column(name="nombres")
	private String nombres;

	@Column(name="apellidos")
	private String apellidos;

	@Column(name="password")
	private String password;

	@Column(name = "fecha_modificacion")
	private LocalDateTime fechaModificacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_empresa")
	private Empresa idEmpresa;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_rol")
	private Rol idRol;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Empresa getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Empresa idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Rol getIdRol() {
		return idRol;
	}

	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}
	
}
