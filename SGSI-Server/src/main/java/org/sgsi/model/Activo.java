package org.sgsi.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="ACTIVO",schema="public")
public class Activo {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbactivo_idactivo_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbactivo_idactivo_seq",
            sequenceName = "tbactivo_idactivo_seq",
            allocationSize = 1
    )
    @Column(name="id_activo", unique=true, nullable=false)
	private int idActivo;

	@Column(name="nombre_activo")
	private String nombreActivo;

	@Column(name="nombre_propetario")
	private String nombrePropetario;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_empresa")
	private Empresa idEmpresa;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_proceso")
	private Proceso idProceso;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_tipo_proceso")
	private TipoActivo idTipoProceso;

	
	public int getIdActivo() {
		return idActivo;
	}
	public void setIdActivo(int idActivo) {
		this.idActivo = idActivo;
	}
	public String getNombreActivo() {
		return nombreActivo;
	}
	public void setNombreActivo(String nombreActivo) {
		this.nombreActivo = nombreActivo;
	}
	public String getNombrePropetario() {
		return nombrePropetario;
	}
	public void setNombrePropetario(String nombrePropetario) {
		this.nombrePropetario = nombrePropetario;
	}
	public Empresa getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Empresa idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public Proceso getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(Proceso idProceso) {
		this.idProceso = idProceso;
	}
	public TipoActivo getIdTipoProceso() {
		return idTipoProceso;
	}
	public void setIdTipoProceso(TipoActivo idTipoProceso) {
		this.idTipoProceso = idTipoProceso;
	}

}
