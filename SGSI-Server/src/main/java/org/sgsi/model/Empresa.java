package org.sgsi.model;

import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="EMPRESA",schema="public")
public class Empresa {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbempresa_idempresa_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbempresa_idempresa_seq",
            sequenceName = "tbempresa_idempresa_seq",
            allocationSize = 1
    )
    @Column(name="id_empresa", unique=true, nullable=false)
	private int idEmpresa;

	@Column(name="nombre")
	private String nombre;

	@Column(name="nit")
	private String nit;

	@Column(name="dui")
	private String dui;
	
	@Column(name="telefono")
	private String telefono;

	@Column(name="direccion")
	private String direccion;

	@Column(name = "fecha_modificacion")
	private LocalDateTime fechaModificacion;

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getDui() {
		return dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
	
}
