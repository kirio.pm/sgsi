package org.sgsi.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="PROCESO",schema="public")
public class Proceso {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbproceso_idproceso_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbproceso_idproceso_seq",
            sequenceName = "tbproceso_idproceso_seq",
            allocationSize = 1
    )
    @Column(name="id_proceso", unique=true, nullable=false)	
	private int idProceso;

	@Column(name="nombre")
	private String nombre;

	@Column(name="descripcion")
	private String descripcion;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_empresa")
	private Empresa idEmpresa;

	public int getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(int idProceso) {
		this.idProceso = idProceso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Empresa getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Empresa idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
}
