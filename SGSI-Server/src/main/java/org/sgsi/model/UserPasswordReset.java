package org.sgsi.model;

import java.time.LocalDateTime;

import javax.persistence.*;

@Entity
@Table(name="USER_PASSWORD_RESET",schema="public")
public class UserPasswordReset {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbuserpasswordreset_iduserpasswordreset_seq")
    @javax.persistence.SequenceGenerator(
            name = "tbuserpasswordreset_iduserpasswordreset_seq",
            sequenceName = "tbuserpasswordreset_iduserpasswordreset_seq",
            allocationSize = 1
    )
    @Column(name="id_uspwdreset", unique=true, nullable=false)
	private int idUserPasswordReset;

	@Column(name = "fecha_modificacion")
	private LocalDateTime fechaModificacion;

	@Column(name = "password_anterior")
	private String passwordAnterior;

	@Column(name = "password_nuevo")
	private String passwordNuevo;

	public int getIdUserPasswordReset() {
		return idUserPasswordReset;
	}

	public void setIdUserPasswordReset(int idUserPasswordReset) {
		this.idUserPasswordReset = idUserPasswordReset;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getPasswordAnterior() {
		return passwordAnterior;
	}

	public void setPasswordAnterior(String passwordAnterior) {
		this.passwordAnterior = passwordAnterior;
	}

	public String getPasswordNuevo() {
		return passwordNuevo;
	}

	public void setPasswordNuevo(String passwordNuevo) {
		this.passwordNuevo = passwordNuevo;
	}
	
}
