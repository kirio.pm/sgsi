package org.sgsi.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.inject.Named;

import org.sgsi.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.view.ViewScoped;
import org.sgsi.model.Empresa;


@Named(value = "empresaViewBean")
@ViewScoped
public class EmpresaBean implements Serializable{

private static final long serialVersionUID = 1L;

@Autowired
EmpresaService empService;

public EmpresaBean() {}

@PostConstruct
public void init() {
	try {
		List<Empresa> ejEmpresa=empService.empresas();
		System.out.println(ejEmpresa.get(0).getNombre());
    } catch (Exception e) {
        System.out.println("Error en postConstruct: " + e);
    }
	}
	public List<Empresa> getlEmpresa() {
		return empService.empresas();
	}

	
}
